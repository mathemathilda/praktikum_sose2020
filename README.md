# Praktikum_SoSe2020
The 'proteomTable.txt' provides the (links to the) sources of the genomic and proteomic data of the species we are interested in. 

Proteomic and genomic data are not available for every species.

You will need to download the *proteome* data.
You will NOT need to download the *genome* data, as we will provide them later in the course. 

In this collection all proteomes carry the "faa" suffix and all genomes the "fna" suffix.

if you find any errors please send a mail to  <gnummig@gmail.com> 
